# postmarketOS skin

This is a skin for the postmarketOS wiki, based off the [Example](https://www.mediawiki.org/wiki/Skin:Example) skin. It is styled after the postmarketOS website.
